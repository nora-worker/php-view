<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\View;

use Nora;

class ViewTest extends \PHPUnit_Framework_TestCase
{
    public function testView ( )
    {
        $renderer = Nora::View_renderer( )->addPath(TEST_PROJECT_PATH.'/views');

        $text = $renderer->twig('hoge.twig', [
            'title' => 'タイトル'
        ]);

        $renderer->ViewModel()->test = 3;
        $renderer->ViewModel()->show = ['vm', function ($vm, $txt) {
            return str_repeat($txt, $vm->test);

        }];

        echo $text = $renderer->php('hoge.php', [
            'title' => 'タイトル'
        ]);
    }
}
