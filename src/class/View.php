<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\View;

use Nora\Core\Module\Module;
use Nora\Core\Component\Component;

/**
 * VIEWモジュール
 */
class View extends Component
{
    const VIEW_CLASS_FORMAT = __namespace__.'\Driver\%s';

    private $_engine;
    private $_loader;

    static public function create ($scope, $setting)
    {
        $class = sprintf(self::VIEW_CLASS_FORMAT, ucfirst($setting->get('driver')));

        $engine = new $class($setting);
        $loader = new Loader\FileSystem($setting->get('paths'));

        return new View($scope, $engine, $loader);
    }

    public function __construct($scope, $engine, $loader)
    {
        $this->_engine = $engine;
        $this->_loader = $loader;

        parent::__construct($scope);
    }

    public function addPath($path)
    {
        $this->_loader->addPath($path);
    }

    public function render($name, $vars = [])
    {
        $vars['vm'] = $this->ViewModel();
        return $this->_engine->render($name, $vars, $this->_loader);
    }

    public function bootViewModel( )
    {
        return new ViewModel($this->scope());
    }
}
