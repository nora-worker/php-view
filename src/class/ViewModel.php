<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\View;

use Nora\Core\Util\Collection\Hash;
use Nora\Core\Component;
use Closure;
use RuntimeException;

/**
 * @author     Hajime MATSUMOTO <hajime.matsumoto@avap.co.jp>
 * @copyright  Since 20014 Nora Project
 * @license    http://nora.avap.co.jp/license.txt
 * @version    $Id:$
 */
class ViewModel
{
    use Component\Componentable;

    private $_reset;
    private $_vars;
    private $_functions;

    protected function initComponentImpl ( )
    {
        $this->_vars = new Hash();
        $this->_functions = new Hash();

        $this->setComponent([
            'html' => function ( ) {
                $helper = $this->html_helper( );
                return $helper;
            },
            'vm' => $this
        ]);

        $this->reset();
    }


    public function reset( )
    {
        $this->_vars->reset();
        $this->_functions->reset();
        return $this;
    }

    public function __set($name, $value)
    {
        if (is_callable($value) || (is_array($value) && is_callable($value[count($value)-1])))
        {
            $this->setHelper($name, $value);
        }
        else
        {
            $this->_vars[$name] = $value;
        }
    }

    public function &__get($name)
    {
        if ($this->_vars->has($name))
        {
            return $this->_vars->get($name);
        }
        $ret = "Undefinedd::$name";

        return $ret;
    }

    public function __isset($name)
    {
        return $this->_functions->has($name) || $this->_vars->has($name);
    }


    public function has($name)
    {
        return $this->_vars->has($name);
    }
}

