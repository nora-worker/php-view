<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\View\Driver;

/**
 * VIEWモジュール
 */
class Php extends Base
{
    public function initComponentImpl()
    {
        $this->initOptions([ ]);
    }

    public function render($name, $vars)
    {
        $____name = $name;
        extract($vars);

        ob_start( );
        include $this->loader()->getFile($____name);
        $data = ob_get_clean();
        return $data;
    }
}

