<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.1.0
 */
namespace Nora\Module\View\Driver;

use Nora\Core\Options\OptionsAccess;
use Nora\Core\Component;

abstract class Base
{
    use Component\Componentable;
    use OptionsAccess;
}


