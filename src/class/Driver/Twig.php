<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\View\Driver;

use Nora\Core\Module\Module;
use Nora\Module\View\Twig\LoaderAdapter;

use Twig_Autoloader;
use Twig_Environment;
use Twig_Lexer;

/**
 * VIEWモジュール
 */
class Twig extends Base
{
    private $_cache;
    private $_options;

    public function initComponentImpl()
    {
        $this->initOptions([
            'lexer' => [],
            'debug' => false
        ]);

        Twig_Autoloader::register();
    }

    public function render($name, $vars)
    {
        $env = new Twig_Environment(new LoaderAdapter($this->loader()), [
            'cache' => $this->Configure()->read('cache', '/tmp/cache').'-view-twig',
            'debug' => $this->getOption('debug')
        ]);

        if ($this->hasOption('lexer'))
        {
            $env->setLexer(
                new Twig_Lexer($env, $this->getOption('lexer'))
            );
        }
        return $env->render($name, $vars);
    }
}

