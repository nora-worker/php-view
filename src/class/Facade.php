<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\View;

use Nora\Core\Module;
use Nora\Core\Options\OptionsAccess;

/**
 * VIEWモジュール
 */
class Facade implements Module\ModuleIF
{
    use Module\Modulable;
    use OptionsAccess;

    private $engine;

    protected function initModuleImpl( )
    {
        $this->initOptions([
            'driver' => [
                'twig' => [
                    'lexer' => [
                        'tag_comment'  => ['{#', '#}'],
                        'tag_block'    => ['{%', '%}'],
                            'tag_variable' => ['{@', '@}']
                    ],
                    'debug' => true
                ],
                'php' => []
            ],
        ]);

        $this->setComponent('View', $this);
    }

    public function bootRenderer( )
    {
        $renderer = new Renderer( );
        $renderer->setScope($this->newScope());
        return $renderer;
    }


    public function create ($input_setting = [], $scope = null)
    {
        if ($scope === null) $scope = $this->newScope();

        $setting = $this->config();
        $setting->merge($input_setting);

        return View::create($scope, $setting);
    }
}
