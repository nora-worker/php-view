<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\View\Loader;

/**
 * ファイルシステムベースのローダ
 */
class FileSystem extends Base
{
    private $_paths;

    public function __construct ($paths = [])
    {
        $this->addPath($paths);
    }

    /**
     * パスを追加する
     */
    public function addPath($path)
    {
        if (is_array($path))
        {
            foreach($path as $v) $this->addPath($v);
            return $this;
        }

        if (func_num_args() > 1)
        {
            foreach(func_get_args() as $v) $this->addPath($v);
            return $this;
        }

        $this->_paths[] = $path;
    }

    /**
     * ファイルを取得する
     */
    public function getFile($name)
    {
        if (false === strpos($name, '.'))
        {
            $name = $name.".php";
        }

        foreach($this->_paths as $p)
        {
            if (file_exists($file =  $p.'/'.$name))
            {
                return $file;
            }
        }
        throw new \RuntimeException('View File Not Found '.$name.' : in '.print_r($this->_paths, true));
    }

    /**
     * ソースを取得
     */
    public function getSource($name)
    {
        if ($this->getFile($name))
        {
            return file_get_contents($this->getFile($name));
        }
        throw new \RuntimeException('Template:'. $name.' not found in:'.print_r($this->_paths, true));
    }

    /**
     * キャッシュキーを取得
     */
    public function getCacheKey($name)
    {
        return $this->getFile($name);
    }

    /**
     * キャッシュ有効性のチェック
     */
    public function isFresh($name, $time)
    {
        return false;
    }
}

