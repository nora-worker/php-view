<?php
/**
 * Nora Project
 *
 * @author Hajime MATSUMOTO <hajime@nora-worker.net>
 * @copyright 2015 nora-worker.net.
 * @licence https://www.nora-worker.net/LICENCE
 * @version 1.0.0
 */
namespace Nora\Module\View;

use Nora\Core\Component;
use Nora\Core\Options\OptionsAccess;

/**
 * VIEW:レンダラー
 */
class Renderer
{
    use Component\Componentable {
        Component\Componentable::__call as private __scope_call;
    }

    public function initComponentImpl( )
    {
        foreach($this->view_getOption('driver', []) as $k=>$v)
        {
            $this->setHelper($k, function () use ($k) {
                $args = func_get_args();
                array_unshift($args, $k);

                return call_user_func_array(
                    [$this, '_render'],
                    $args
                );

            });

            $this->setComponent('_driver_'.$k, function ( ) use ($k, $v) {
                return $this->createDriver($k, $v);
            });
        }

        $this->setComponent([
            '@loader' =>  function ( ) {
                $loader = new Loader\FileSystem();
                return $loader;
            },
            '@ViewModel' => function ( ) {
                $vm = new ViewModel();
                $vm->setScope($this->newScope());
                return $vm;
            }
        ]);
    }


    public function addPath($path)
    {
        $this->getScope()->loader( )->addPath($path);
        return $this;
    }

    public function createDriver($name, $settings)
    {
        $class = sprintf(__namespace__.'\\Driver\\%s', ucfirst($name));
        $engine = new $class( );
        $engine->setScope($this->newScope());
        $engine->setOption($settings);
        return $engine;
    }

    private function _render ($driver, $name, $values)
    {
        $values['vm'] = $this->ViewModel();
        return $this->getComponent('_driver_'.$driver)->render($name, $values);
    }

}
